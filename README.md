# Lekcja 1
## Zadanie 2 - pro
Umieść w pliku `index.html` dwa tagi `<script>`. Pierwszy na końcu sekcji `<head>`, drugi natomiast na przed zamykając tagiem `</body>`. Dodatkowo drugi tag ma mieć identyfikator `"test-script"` oraz ustawiony atrybut `src` na plik `index.js` (plik ten nie musi istnieć).
