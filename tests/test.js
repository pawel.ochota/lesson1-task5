const Differencify = require('differencify');
const differencify = new Differencify({ mismatchThreshold: 0 });
let urlToTest = 'http://127.0.0.1:8080/';

describe('HTML page', () => {
  const timeout = 30000;
  let page;

  beforeAll(async () => {
    await differencify.launchBrowser({
      args: ['--no-sandbox', '--disable-setuid-sandbox']
    });
    const target = differencify.init({ chain: false });
    page = await target.newPage();
    await page.goto(urlToTest);
    await page.waitFor(1000);
  }, timeout);
  afterAll(async () => {
    await differencify.cleanup();
  });

  it('should have two <script> tags', async () => {
    const scripts = await page.$$eval('script', scripts => scripts.length);
    expect(scripts).toBe(3); // 3 because the extra one is from the puppeteer environment
  }, timeout);

  it('should have one <script> tag at the end of <head> section', async () => {
    const script = await page.$$eval('head script:last-of-type', script => script.length);
    expect(script).toBe(1);
  }, timeout);

  it('should have second <script> tag at the end of <body> section with test-script id and index.js source', async () => {
    const source = await page.$eval('#test-script', script => script.src);
    expect(source.endsWith('index.js')).toBe(true);
  }, timeout);
});
